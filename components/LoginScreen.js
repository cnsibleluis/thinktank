import React, {Component} from 'react';
import {Image as ReactImage} from 'react-native';
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity
} from 'react-native';
import styles from "../styles/loginScreenStyles";
import Svg, {Defs, Pattern} from 'react-native-svg';
import {Path as SvgPath} from 'react-native-svg';
import {Text as SvgText} from 'react-native-svg';
import {Image as SvgImage} from 'react-native-svg';

export default class LoginScreen extends Component {

    onFacebookBtnPress = () => {

    }

    onGoogleBtnPress = () => {

    }

    onEmailBtnPress = () => {

    }

    render() {
        return (
            <ScrollView style={styles.loginScreen}>
                <View style={styles.headerView}>
                    <ReactImage source={require('../assets/header-design.png')} style={styles.headerImage}/>
                </View>
                <View>
                    <ReactImage source={require('../assets/thinktank-logo.png')} style={styles.logo}/>
                </View>

                <View style={styles.buttonGroup}>
                    <View style={styles.facebookBtnView}>
                        <TouchableOpacity style={styles.facebookBtn} onPress={this.onFacebookBtnPress()}>
                            <View>
                                <Svg style={styles.facebookLogo} preserveAspectRatio="none" viewBox="80 0 11.511138916015625 22.0426025390625" fill="rgba(255, 255, 255, 1)">
                                    <SvgPath d="M 87.46999359130859 22.04258918762207 L 87.46999359130859 12.00096416473389 L 90.89884948730469 12.00096416473389 L 91.38868713378906 8.082283973693848 L 87.46999359130859 8.082283973693848 L 87.46999359130859 5.633106231689453 C 87.46999359130859 4.530977249145508 87.83738708496094 3.673765659332275 89.42934417724609 3.673765659332275 L 91.51113891601563 3.673765659332275 L 91.51113891601563 0.1224588304758072 C 91.02129364013672 0.1224588304758072 89.79670715332031 0 88.44965362548828 0 C 85.51065063476563 0 83.42884826660156 1.836882472038269 83.42884826660156 5.143270492553711 L 83.42884826660156 8.082283020019531 L 80 8.082283020019531 L 80 12.00096416473389 L 83.42884826660156 12.00096416473389 L 83.42884826660156 22.04258918762207 L 87.46999359130859 22.04258918762207 Z"  />
                                </Svg>
                            </View>
                            <Text style={styles.facebookBtnText}>Login with Facebook</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.googleBtnView}>
                        <TouchableOpacity style={styles.googleBtn} onPress={this.onGoogleBtnPress()}>
                            <Svg style={styles.googleLogoEllipse} preserveAspectRatio="none" viewBox="-0.75 -0.75 35.5 35.5" fill="rgba(255, 255, 255, 1)"><SvgPath d="M 17 0 C 26.38883972167969 0 34 7.611159324645996 34 17 C 34 26.38883972167969 26.38883972167969 34 17 34 C 7.611159324645996 34 0 26.38883972167969 0 17 C 0 7.611159324645996 7.611159324645996 0 17 0 Z"  /></Svg>
                            <View style={styles.googleColors}>
                                <Svg style={styles.googleLogo_1} preserveAspectRatio="none" viewBox="272.0999755859375 223.0999755859375 8.65313720703125 8.45794677734375" fill="rgba(66, 133, 244, 1)"><SvgPath d="M 280.7531127929688 224.9306030273438 C 280.7531127929688 224.3181762695313 280.7035522460938 223.7024841308594 280.5975952148438 223.0999755859375 L 272.0999755859375 223.0999755859375 L 272.0999755859375 226.5692138671875 L 276.9661865234375 226.5692138671875 C 276.7642211914063 227.6880798339844 276.1153564453125 228.6779174804688 275.1653442382813 229.3068542480469 L 275.1653442382813 231.5578918457031 L 278.0685424804688 231.5578918457031 C 279.7733154296875 229.98876953125 280.7531127929688 227.6715393066406 280.7531127929688 224.9306030273438 Z"  /></Svg>
                                <Svg style={styles.googleLogo_2} preserveAspectRatio="none" viewBox="28.89999771118164 324.2999572753906 14.022544860839844 7.282745361328125" fill="rgba(52, 168, 83, 1)"><SvgPath d="M 36.95071792602539 331.5827026367188 C 39.3804931640625 331.5827026367188 41.42958831787109 330.784912109375 42.92254638671875 329.4077758789063 L 40.01938629150391 327.1567687988281 C 39.21166610717773 327.7062683105469 38.16891479492188 328.0174255371094 36.95402526855469 328.0174255371094 C 34.60369110107422 328.0174255371094 32.61087417602539 326.4317932128906 31.89584732055664 324.2999572753906 L 28.89999771118164 324.2999572753906 L 28.89999771118164 326.6204833984375 C 30.42936706542969 329.6626892089844 33.54438400268555 331.5827026367188 36.95071792602539 331.5827026367188 Z"  /></Svg>
                                <Svg style={styles.googleLogo_3} preserveAspectRatio="none" viewBox="-0.04999929666519165 150 3.9508819580078125 8.09039306640625" fill="rgba(251, 188, 4, 1)"><SvgPath d="M 3.900876522064209 155.7698669433594 C 3.523499011993408 154.6509857177734 3.523499011993408 153.4394226074219 3.900876522064209 152.3205261230469 L 3.900876522064209 150 L 0.9083405137062073 150 C -0.3694458305835724 152.5456237792969 -0.3694458305835724 155.5447845458984 0.9083405137062073 158.0904083251953 L 3.900876522064209 155.7698669433594 Z"  /></Svg>
                                <Svg style={styles.googleLogo_4} preserveAspectRatio="none" viewBox="28.89999771118164 -0.018340587615966797 14.078826904296875 7.286651611328125" fill="rgba(234, 67, 53, 1)"><SvgPath d="M 36.95071792602539 3.547488451004028 C 38.23512268066406 3.527626752853394 39.47649765014648 4.010934352874756 40.40669631958008 4.898102283477783 L 40.40669631958008 4.898102283477783 L 42.97882461547852 2.32597827911377 C 41.35013961791992 0.7966068983078003 39.18849182128906 -0.0442163273692131 36.95071792602539 -0.01773370802402496 C 33.54438400268555 -0.01773370802402496 30.42936706542969 1.90225613117218 28.89999771118164 4.947757244110107 L 31.89253234863281 7.268296718597412 C 32.60425567626953 5.133135795593262 34.60037994384766 3.547488451004028 36.95071792602539 3.547488451004028 Z"  /></Svg>
                            </View>
                            <Text style={styles.googleBtnText}>
                                Login with Google
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.orGroup}>
                        <View style={styles.orLine}></View>
                        <Text style={styles.orText}>OR</Text>
                        <View style={styles.orLine}></View>
                    </View>

                    <View style={styles.emailBtnView}>
                        <TouchableOpacity style={styles.emailBtn} onPress={this.onEmailBtnPress()}>
                            <Svg style={styles.emailLogo} preserveAspectRatio="none" viewBox="5.5 18.5 28.5 21.73468017578125" fill="white"><SvgPath d="M 7 20 L 32.5 20 L 32.5 38.73469543457031 L 7 38.73469543457031 L 7 20 Z M 7 20 L 18.4489803314209 31.44897842407227 C 19.31632614135742 32.14285659790039 20.18367385864258 32.14285659790039 21.0510196685791 31.44897842407227 L 32.5 20 M 7 38.73469543457031 L 16.36734771728516 29.36734771728516 M 23.13265419006348 29.36734771728516 L 32.5 38.73469543457031"  /></Svg>
                            <Text style={styles.emailText}>Login with Email</Text>
                           </TouchableOpacity>
                       </View>
                </View>

            </ScrollView>
        );
    }
}
