import {StyleSheet} from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp
} from "react-native-responsive-screen";

const styles = StyleSheet.create({
    loginScreen: {
        opacity: 1,
        position: "absolute",
        backgroundColor: "rgba(255, 255, 255, 1)",
        height: hp(100),
        width: wp(100),
    },
    headerView: {
        opacity: 1,
        backgroundColor: "transparent",
    },
    headerImage: {
        height: hp(20),
        width: wp(100),
    },
    logo: {
        top: hp(-11),
        left: wp(25),
        width: wp(52),
        height: hp(30),
    },
    buttonGroup: {

    },
    facebookBtnView: {
        left: wp(25),
        paddingBottom: 10
    },
    facebookBtn: {
        backgroundColor: "rgba(51, 53, 153, 1)",
        borderTopWidth: 1,
        borderTopColor: "rgba(112, 112, 112, 1)",
        borderRightWidth: 1,
        borderRightColor: "rgba(112, 112, 112, 1)",
        borderBottomWidth: 1,
        borderBottomColor: "rgba(112, 112, 112, 1)",
        borderLeftWidth: 1,
        borderLeftColor: "rgba(112, 112, 112, 1)",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        width: 230,
        height: 40,
    },
    facebookBtnText: {
        backgroundColor: "rgba(255, 255, 255, 0)",
        color: "rgba(255, 255, 255, 1)",
        fontSize: 16,
        fontWeight: "400",
        fontStyle: "normal",
        fontFamily: "segoe-ui",
        left: 62,
        top: 9,
    },
    facebookLogo: {
        position: "absolute",
        backgroundColor: "transparent",
        width: 11.51,
        height: 22.04,
        left: 16,
        top: 9
    },
    googleBtnView: {
        left: wp(25),
        paddingBottom: 10
    },
    googleBtn: {
        backgroundColor: "rgba(150, 150, 150, 1)",
        borderTopColor: "rgba(112, 112, 112, 1)",
        borderRightWidth: 1,
        borderRightColor: "rgba(112, 112, 112, 1)",
        borderBottomWidth: 1,
        borderBottomColor: "rgba(112, 112, 112, 1)",
        borderLeftWidth: 1,
        borderLeftColor: "rgba(112, 112, 112, 1)",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: "rgb(0,  0,  0)",
        shadowOpacity: 0.1607843137254902,
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowRadius: 6,
        width: 230,
        height: 40,
    },
    googleBtnText: {
        backgroundColor: "rgba(255, 255, 255, 0)",
        color: "rgba(255, 255, 255, 1)",
        fontSize: 16,
        fontWeight: "400",
        fontStyle: "normal",
        fontFamily: "segoe-ui",
        textAlign: "left",
        left: 65,
        top: 8
    },
    googleLogoEllipse: {
        position: "absolute",
        "width": 34,
        "height": 34,
        "left": 5,
        "top": 3
    },
    googleColors: {
        top: 11,
        left: 13,
    },
    googleLogo_1: {
        position: "absolute",
        "width": 8.65,
        "height": 8.46,
        "left": 9.01,
        "top": 7.39
    },
    googleLogo_2: {
        position: "absolute",
        "width": 14.02,
        "height": 7.28,
        "left": 0.96,
        "top": 10.74
    },
    googleLogo_3: {
        position: "absolute",
        "width": 3.95,
        "height": 8.09,
        "left": 0,
        "top": 4.97
    },
    googleLogo_4: {
        position: "absolute",
        "width": 14.08,
        "height": 7.29,
        "left": 0.96,
        "top": 0
    },
    orGroup: {
        flexDirection: "row",
        left: wp(25),
        paddingBottom: 10
    },
    orLine: {
        borderBottomColor: "black",
        width: "25%",
        borderBottomWidth: StyleSheet.hairlineWidth,
        alignSelf: "stretch",
        marginBottom: 10,
    },
    orText: {
        backgroundColor: "rgba(255, 255, 255, 0)",
        color: "rgba(0, 0, 0, 1)",
        fontSize: 16,
        fontWeight: "400",
        fontStyle: "normal",
        fontFamily: "helvetica-neue",
    },
    emailBtnView: {
        left: wp(25),
    },
    emailBtn: {
        "backgroundColor": "rgba(1, 176, 241, 1)",
        "borderTopWidth": 1,
        "borderTopColor": "rgba(112, 112, 112, 1)",
        "borderRightWidth": 1,
        "borderRightColor": "rgba(112, 112, 112, 1)",
        "borderBottomWidth": 1,
        "borderBottomColor": "rgba(112, 112, 112, 1)",
        "borderLeftWidth": 1,
        "borderLeftColor": "rgba(112, 112, 112, 1)",
        "borderTopLeftRadius": 10,
        "borderTopRightRadius": 10,
        "borderBottomLeftRadius": 10,
        "borderBottomRightRadius": 10,
        "shadowColor": "rgb(0,  0,  0)",
        "shadowOpacity": 0.1607843137254902,
        "shadowOffset": {
            "width": 0,
            "height": 4
        },
        "shadowRadius": 6,
        "width": 230,
        "height": 40,
    },
    emailLogo: {
        backgroundColor: "rgb(194,194,194)",
        position: "absolute",
        "width": 29.5,
        "height": 22.73,
        "left": 10.5,
        "top": 8.5
    },
    emailText: {
        "backgroundColor": "rgba(255, 255, 255, 0)",
        "color": "rgba(255, 255, 255, 1)",
        "fontSize": 16,
        "fontWeight": "400",
        "fontStyle": "normal",
        "fontFamily": "segoe-ui",
        "textAlign": "left",
        "left": 77,
        "top": 8
    }
});

export default  styles;
